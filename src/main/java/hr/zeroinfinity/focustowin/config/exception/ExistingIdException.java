package hr.zeroinfinity.focustowin.config.exception;

public class ExistingIdException extends RuntimeException {

    public ExistingIdException(){}

    public ExistingIdException(String message){
        super(message);
    }

}
