package hr.zeroinfinity.focustowin.config.exception.handler;

import hr.zeroinfinity.focustowin.config.exception.EntityNotFoundException;
import hr.zeroinfinity.focustowin.data.model.dto.ResponseDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.exceptions.UnauthorizedUserException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestResponseEntityExceptionHandler.class);
    private static final String ERROR_LOG = "Request failed for uri: %s, error description: %s.";
    private static final String ENTITY_NOT_FOUND_DEF_MESS = "Entity with given id not found.";
    private static final String ILLEGAL_ARGUMENTS_DEF_MESS = "Illegal arguments provided for called method";

    @ExceptionHandler(value = {EntityNotFoundException.class})
    protected ResponseEntity<Object> handleEntityNotFound(RuntimeException ex, WebRequest request) {
        LOGGER.info(String.format(ERROR_LOG, request.getDescription(true), ex.getMessage()));

        ResponseDTO dto = new ResponseDTO();
        dto.setMessage(ex.getMessage() == null ? ENTITY_NOT_FOUND_DEF_MESS : ex.getMessage());
        dto.setStatus(HttpStatus.NOT_FOUND.value());

        return handleExceptionInternal(ex, dto, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler(value = {UnauthorizedUserException.class})
    protected ResponseEntity<Object> handleUnauthorizedUser(RuntimeException ex, WebRequest request) {
        LOGGER.info(String.format(ERROR_LOG, request.getDescription(true), ex.getMessage()));

        ResponseDTO dto = new ResponseDTO();
        dto.setMessage(ex.getMessage());
        dto.setStatus(HttpStatus.UNAUTHORIZED.value());

        return handleExceptionInternal(ex, dto, new HttpHeaders(), HttpStatus.FORBIDDEN, request);
    }

    @ExceptionHandler(value = {IllegalArgumentException.class})
    protected ResponseEntity<Object> handleIllegalArgument(RuntimeException ex, WebRequest request) {
        LOGGER.info(String.format(ERROR_LOG, request.getDescription(true), ex.getMessage()));


        ResponseDTO dto = new ResponseDTO();
        dto.setMessage(ex.getMessage() == null ? ILLEGAL_ARGUMENTS_DEF_MESS : ex.getMessage());
        dto.setStatus(HttpStatus.BAD_REQUEST.value());

        return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }
}