package hr.zeroinfinity.focustowin.config.exception.supplier;

import hr.zeroinfinity.focustowin.config.exception.EntityNotFoundException;
import hr.zeroinfinity.focustowin.config.exception.ExistingIdException;
import hr.zeroinfinity.focustowin.config.exception.IdNonExistantError;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import java.util.function.Supplier;

public class ExceptionSuppliers {

    /**
     * Used when trying to find a user with a non-existing username
     */
    public static final Supplier<UsernameNotFoundException> USERNAME_NOT_FOUND = () -> new UsernameNotFoundException("User with provided email has not been registered.");

    /**
     * Used to throw exceptions while trying to fetch an entity that doesn't exist in the database
     */
    public static final Supplier<EntityNotFoundException> ENTITY_NOT_FOUND = EntityNotFoundException::new;

    /**
     * Used when trying to update a non-existing chore through the event update endpoint
     */
    public static final Supplier<EntityNotFoundException> ENTITY_NOT_FOUND_CHORE = () -> new EntityNotFoundException("Chore ID must not be null");

}
