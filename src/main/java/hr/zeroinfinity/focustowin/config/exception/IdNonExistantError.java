package hr.zeroinfinity.focustowin.config.exception;

public class IdNonExistantError extends RuntimeException{

    public IdNonExistantError(){}

    public IdNonExistantError(String message){
        super(message);
    }
}
