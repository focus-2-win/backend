//package hr.zeroinfinity.focustowin.config;
//
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.http.HttpStatus;
//
//import javax.servlet.Filter;
//import javax.servlet.FilterChain;
//import javax.servlet.FilterConfig;
//import javax.servlet.ServletException;
//import javax.servlet.ServletRequest;
//import javax.servlet.ServletResponse;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//
//@Configuration
//public class ApiKeyFilter implements Filter {
//
//    private static final String API_KEY_HEADER = "F2W-Api-Key";
//
//    @Value("${app.security.api-key}")
//    private String API_KEY;
//
//    @Override
//    public void init(FilterConfig filterConfig) {
//    }
//
//    @Override
//    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
//        HttpServletRequest request = (HttpServletRequest) req;
//        HttpServletResponse response = (HttpServletResponse) res;
//        String apiKey = request.getHeader(API_KEY_HEADER);
//
//        if (apiKey != null && apiKey.equals(API_KEY)) {
//            chain.doFilter(req, res);
//        } else {
//            response.sendError(HttpStatus.FORBIDDEN.value());
//        }
//    }
//
//    @Override
//    public void destroy() {
//    }
//}
