package hr.zeroinfinity.focustowin.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;

@EnableResourceServer
@Configuration
public class ResourceServer extends ResourceServerConfigurerAdapter {

    private static final String[] PUBLIC_ROUTES = {"/swagger-ui.html",
            "/webjars/**",
            "/h2-console/**",
            "/swagger-resources/**",
            "/",
            "/csrf",
            "/v2/api-docs",
            "/css/**",
            "/js/**",
            "/img/**",
            "/**/favicon.ico",
            "/actuator/**",
            "/actuator",
            "/account/confirm",
            "/user/register",
            "/user/forgotPassword",
            "/info/*",
            "/api/v1/user**",
            "/api/v1/family**",
            "/api/v1/child**",
            "/api/v1/wish**",
            "/api/v1/event**",
            "/api/v1/chore**"
    }; //TODO: close endpoints on release

    private final DefaultTokenServices tokenServices;

    @Autowired
    public ResourceServer(DefaultTokenServices tokenServices) {
        this.tokenServices = tokenServices;
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.headers().frameOptions().sameOrigin();

        http.
                authorizeRequests()
                .anyRequest()
                .permitAll();
        // TODO : revisit when finished with testing
//                .antMatchers(PUBLIC_ROUTES).permitAll()
//                .and().authorizeRequests().anyRequest().authenticated();
    }

    @Override
    public void configure(ResourceServerSecurityConfigurer config) {
        config.tokenServices(tokenServices);
    }
}
