package hr.zeroinfinity.focustowin.config.security.jwt;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.security.Principal;

@Data
@AllArgsConstructor
public class UserPrincipal implements Principal, Serializable {

    private UserTokenData userTokenData;
    private String name;
}