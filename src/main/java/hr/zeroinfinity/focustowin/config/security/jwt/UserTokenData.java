package hr.zeroinfinity.focustowin.config.security.jwt;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
public class UserTokenData {
    private Long id;
}
