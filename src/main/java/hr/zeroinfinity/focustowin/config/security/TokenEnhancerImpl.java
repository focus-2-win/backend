package hr.zeroinfinity.focustowin.config.security;

import hr.zeroinfinity.focustowin.config.security.jwt.UserTokenData;
import hr.zeroinfinity.focustowin.data.model.User;
import hr.zeroinfinity.focustowin.data.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static hr.zeroinfinity.focustowin.config.exception.supplier.ExceptionSuppliers.ENTITY_NOT_FOUND;

@Component
@Primary
public class TokenEnhancerImpl implements TokenEnhancer {

    @Value("${app.security.jwt-data-key}")
    private String USER_DATA_KEY;

    private final UserRepository userRepository;

    @Autowired
    public TokenEnhancerImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
        final Map<String, Object> additionalInfo = new HashMap<>();
        UserTokenData userTokenData = getUserData(authentication);

        additionalInfo.put(USER_DATA_KEY, userTokenData);

        ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo);
        return accessToken;
    }

    private UserTokenData getUserData(OAuth2Authentication authentication) {
        User user = userRepository.findByEmail(authentication.getName()).orElseThrow(ENTITY_NOT_FOUND);
        UserTokenData userTokenData = new UserTokenData();

        userTokenData.setId(user.getId());

        return userTokenData;
    }
}