package hr.zeroinfinity.focustowin.controller;

import hr.zeroinfinity.focustowin.data.model.dto.ChildDTO;
import hr.zeroinfinity.focustowin.data.model.dto.ResponseDTO;
import hr.zeroinfinity.focustowin.service.ChildService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/api/v1/child")
@Api(value="/api/v1/child", description = "Child operations")
public class ChildController extends AbstractController {

    private final ChildService childService;

    @Autowired
    public ChildController(ChildService childService, MessageSource messageSource) {
        super(messageSource);

        this.childService = childService;
    }

    @ApiOperation(value = "Fetch a ChildDTO object pertaining to a Child with the given ID", response = ChildDTO.class)
    @GetMapping(path = "", produces = "application/json")
    public ResponseEntity<ChildDTO> get(@ApiParam(name="childId", value = "ID of the child we want to fetch", defaultValue = "1") @RequestParam("childId") @NotNull Long childId){
        return ResponseEntity.ok().body(childService.fetchChild(childId));
    }

    @ApiOperation(value = "Fetch a ChildDTO of a new Child with only the future child ID set. This is used for QR code generation on the child version of the app", response = ChildDTO.class)
    @GetMapping(path = "/new", produces = "application/json")
    public ResponseEntity<ChildDTO> getNew(){
        return ResponseEntity.ok().body(childService.createChild());
    }

    @ApiOperation(value = "Update information about the Child given via ChildDTO. \n Also used for entering valid child data for the first time (see parameter description)")
    @PutMapping(path = "", produces = "application/json")
    public ResponseEntity<ResponseDTO<Long>> update(@ApiParam(name = "childDTO", value = "ChildDTO object which contains child data we want to update. " +
            "\n Also used for entering a newly created child with the id given on the /new endpoint")
                                                        @RequestBody @Valid ChildDTO childDTO, BindingResult bindingResult) {
        if(bindingResult.hasErrors()){
            return ResponseEntity.unprocessableEntity().body(createErrorResponse(bindingResult));
        }
        ChildDTO c = childService.updateChild(childDTO);
        return ResponseEntity.ok().body(new ResponseDTO<>(c.getId()));
    }

    @ApiOperation(value = "Delete a child using its ID")
    @DeleteMapping(path = "", produces = "application/json")
    public ResponseEntity<ResponseDTO> delete(@ApiParam(name = "childId", value = "Id of the child we want to delete", defaultValue = "0") @RequestParam("childId") Long childId) {
        childService.removeChild(childId);
        return ResponseEntity.ok(new ResponseDTO());
    }
}
