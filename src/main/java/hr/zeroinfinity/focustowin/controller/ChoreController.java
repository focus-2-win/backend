package hr.zeroinfinity.focustowin.controller;

import hr.zeroinfinity.focustowin.data.model.dto.ChoreDTO;
import hr.zeroinfinity.focustowin.data.model.dto.ResponseDTO;
import hr.zeroinfinity.focustowin.service.ChoreService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/api/v1/chore")
@Api(value="/api/v1/chore", description = "Chore operations")
public class ChoreController extends AbstractController {

    private final ChoreService choreService;

    @Autowired
    public ChoreController(MessageSource messageSource, ChoreService choreService){
        super(messageSource);
        this.choreService = choreService;
    }

    @ApiOperation(value = "Update information about a single chore via ChoreDTO. This endpoint is optional, chore updates should primaraly be done through the PUT EVENT endpoint")
    @PutMapping(path = "", produces = "application/json")
    public ResponseEntity<ResponseDTO<Long>> update(@ApiParam(name = "choreDTO", value = "ChoreDTO which contains chore data that we want to update") @RequestBody @Valid ChoreDTO choreDTO, BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            return ResponseEntity.unprocessableEntity().body(createErrorResponse(bindingResult));
        }
        ChoreDTO c = choreService.updateChore(choreDTO);
        return ResponseEntity.ok().body(new ResponseDTO<>(c.getId()));
    }

    @ApiOperation(value = "Delete a chore using its ID")
    @DeleteMapping(path = "", produces = "application/json")
    public ResponseEntity<ResponseDTO> delete(@ApiParam(name = "id", value = "ID of the chore we wish to delte")@RequestParam @NotNull Long id){
        choreService.deleteChore(id);
        return ResponseEntity.ok().body(new ResponseDTO());
    }
}

