package hr.zeroinfinity.focustowin.controller;

import hr.zeroinfinity.focustowin.data.model.Event;
import hr.zeroinfinity.focustowin.data.model.dto.ChildDTO;
import hr.zeroinfinity.focustowin.data.model.dto.EventDTO;
import hr.zeroinfinity.focustowin.data.model.dto.ResponseDTO;
import hr.zeroinfinity.focustowin.service.EventService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/api/v1/event")
@Api(value="/api/v1/event", description = "Event operations")
public class EventController extends AbstractController {

    private final EventService eventService;

    @Autowired
    public EventController(MessageSource messageSource, EventService eventService){
        super(messageSource);
        this.eventService = eventService;
    }

    @ApiOperation(value = "Fetch a pageable object containing all Events belonging to a child with the given ID")
    @GetMapping(path = "", produces = "application/json")
    public ResponseEntity<Page<EventDTO>> getPageable(
            @ApiParam(name = "childId", value = "ID of the child whose events we want to fetch") @RequestParam @NotNull Long childId,
            @RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "20") Integer pageSize){
        return ResponseEntity.ok().body(eventService.getEventsForChild(childId, page, pageSize));
    }

    @ApiOperation(value = "Create new event with information stored via EventDTO and save it in the DB")
    @PostMapping(path = "", produces = "application/json")
    public ResponseEntity<ResponseDTO<Long>> create(@ApiParam(name = "eventDTO", value = "EventDTO containing data about the event we wish to create")@RequestBody @Valid EventDTO eventDTO, BindingResult bindingResult){
        EventDTO eventDto = eventService.createEvent(eventDTO);
        return ResponseEntity.ok().body(new ResponseDTO<>(eventDto.getId()));
    }

    @ApiOperation(value = "Update an existing event via EventDTO. Also used to complete an event and calculate points if the event is complete( EventStatus = SUCCEEDED )")
    @PutMapping(path = "", produces = "application/json")
    public ResponseEntity<ResponseDTO<Long>> update(@ApiParam(name = "eventDTO", value = "EventDTO containing data about the event we wish to update")@RequestBody @Valid EventDTO eventDTO, BindingResult bindingResult){
        EventDTO event = eventService.updateEvent(eventDTO);
        return ResponseEntity.ok().body(new ResponseDTO<>(event.getId()));
    }

    @ApiOperation(value = "Delete and existing event using its ID")
    @DeleteMapping(path = "", produces = "application/json")
    public ResponseEntity<ResponseDTO> delete(@ApiParam(name = "id", value = "ID of the event we want to delete") @RequestParam @NotNull Long id){
        EventDTO eventDTO = eventService.deleteEvent(id);
        return ResponseEntity.ok().body(new ResponseDTO<>(eventDTO));
    }
}
