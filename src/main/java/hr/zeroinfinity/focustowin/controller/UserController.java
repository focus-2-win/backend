package hr.zeroinfinity.focustowin.controller;

import hr.zeroinfinity.focustowin.data.model.User;
import hr.zeroinfinity.focustowin.data.model.dto.ResponseDTO;
import hr.zeroinfinity.focustowin.data.model.dto.UserDTO;
import hr.zeroinfinity.focustowin.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/user")
@Api(value="/api/v1/user", description = "User operations")
public class UserController extends AbstractController{

    private final UserService userService;

    @Autowired
    public UserController(UserService userService, MessageSource messageSource) {
        super(messageSource);

        this.userService = userService;
    }

    @ApiOperation(value = "Register a new user")
    @PostMapping(value = "", produces = "application/json")
    public ResponseEntity<ResponseDTO<Long>> registerNewUser(@ApiParam(name = "userDTO", value = "UserDTO containing data pertaining to the new user") @RequestBody @Valid UserDTO userDTO, BindingResult bindingResult) {
        if(bindingResult.hasErrors()){
            return ResponseEntity.ok(createErrorResponse(bindingResult));
        }
        return ResponseEntity.ok(new ResponseDTO<Long>(userService.register(userDTO).getId()));
    }

    @ApiOperation(value =  "Generate a new password and send it via email (NOT IMPLEMENTED)")
    @GetMapping(value = "/forgotPassword", produces = "application/json")
    public ResponseEntity<ResponseDTO> resetPassword(@RequestBody String email){
        //TODO generate random password and send it do the email
        //first check if email is in our system
        return ResponseEntity.notFound().build();
    }
}
