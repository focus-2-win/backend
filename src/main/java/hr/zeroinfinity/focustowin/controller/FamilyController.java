package hr.zeroinfinity.focustowin.controller;

import hr.zeroinfinity.focustowin.data.model.Family;
import hr.zeroinfinity.focustowin.data.model.dto.ChildDTO;
import hr.zeroinfinity.focustowin.data.model.dto.FamilyDTO;
import hr.zeroinfinity.focustowin.data.model.dto.RequestDTO;
import hr.zeroinfinity.focustowin.data.model.dto.UserDTO;
import hr.zeroinfinity.focustowin.service.FamilyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/api/v1/family")
@Api(value="/api/v1/family", description = "Family operations")
public class FamilyController extends AbstractController {

    private final FamilyService familyService;

    @Autowired
    public FamilyController(MessageSource messageSource, FamilyService familyService) {
        super(messageSource);
        this.familyService = familyService;
    }

    @ApiOperation(value = "Create a new Family using active users credentials")
    @PostMapping(path = "", produces = "application/json")
    public ResponseEntity<FamilyDTO> createFamily() {
        return ResponseEntity.ok(familyService.create());
    }

    @ApiOperation(value = "Fetch the current users family using its credentials")
    @GetMapping(value = "/parent", produces = "application/json")
    public ResponseEntity<FamilyDTO> getFamily(){
        return ResponseEntity.ok().body(familyService.get());
    }

    @ApiOperation(value = "Add a new parent to the family using its ID (QR code)")
    @PostMapping(path = "/parent", produces = "application/json")
    public ResponseEntity addParentToFamily(@ApiParam(name = "request", value = "ID of the user we want to add to the family as a parent")@RequestBody @Valid RequestDTO request, BindingResult bindingResult) {
        if(bindingResult.hasErrors()){
            return ResponseEntity.unprocessableEntity().body(createErrorResponse(bindingResult));
        }
        return ResponseEntity.ok().body(familyService.addParent(request.getEntityId()));
    }

    @ApiOperation(value = "Fetch the Family which contains the child with the given ID")
    @GetMapping(path = "/child", produces = "application/json")
    public ResponseEntity<FamilyDTO> getFamilyByChildId(@ApiParam(name ="childId", value="ID of the child whose family we want to fetch")@RequestParam @NotNull Long childId){
        return ResponseEntity.ok().body(familyService.findFamilyByChild(childId));
    }

    @ApiOperation(value = "Add a new child to the family using ChildDTO")
    @PostMapping(path = "/child", produces = "application/json")
    public ResponseEntity<ChildDTO> addChildToFamily(@ApiParam(name = "childDTO", value = "Child we want to add to the family") @RequestBody ChildDTO childDTO) {
        return ResponseEntity.ok().body(familyService.addChild(childDTO));
    }

}
