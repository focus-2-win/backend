package hr.zeroinfinity.focustowin.controller;

import hr.zeroinfinity.focustowin.data.model.dto.ResponseDTO;
import hr.zeroinfinity.focustowin.data.model.dto.WishDTO;
import hr.zeroinfinity.focustowin.service.WishService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/api/v1/wish")
@Api(value="/api/v1/wish", description = "Wish operations")
public class WishController extends AbstractController {

    private final WishService wishService;

    @Autowired
    public WishController(MessageSource messageSource,WishService wishService){
        super(messageSource);
        this.wishService = wishService;
    }

    @ApiOperation("Create a new wish and save it in the DB")
    @PostMapping(path = "", produces = "application/json")
    public ResponseEntity<ResponseDTO> create(@ApiParam(name = "wishDTO") @RequestBody @Valid WishDTO wishDTO, BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            return ResponseEntity.unprocessableEntity().body(createErrorResponse(bindingResult));
        }

        WishDTO w = wishService.createWish(wishDTO);
        return ResponseEntity.ok().body(new ResponseDTO<Long>(w.getId()));
    }

    @ApiOperation("Update an existing wish. Also used to complete the wish")
    @PutMapping(path = "", produces = "application/json")
    public ResponseEntity<ResponseDTO<Long>> update(@ApiParam(name = "wishDTO") @RequestBody @Valid WishDTO wishDTO, BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            return ResponseEntity.unprocessableEntity().body(createErrorResponse(bindingResult));
        }

        WishDTO w  = wishService.updateWish(wishDTO);
        return ResponseEntity.ok().body(new ResponseDTO<Long>(w.getId()));
    }

    @ApiOperation(value =  "Delete an existing wish")
    @DeleteMapping(path = "", produces = "application/json")
    public ResponseEntity<ResponseDTO> delete(@ApiParam(name = "id", value = "ID of the wish we want to delete") @RequestParam @NotNull Long id){
        wishService.deleteWish(id);
        return ResponseEntity.ok().body(new ResponseDTO());
    }
}
