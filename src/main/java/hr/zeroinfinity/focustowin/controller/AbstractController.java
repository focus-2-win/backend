package hr.zeroinfinity.focustowin.controller;

import hr.zeroinfinity.focustowin.data.model.dto.ResponseDTO;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.Locale;

public abstract class AbstractController {

    private MessageSource messageSource;

    protected AbstractController(MessageSource messageSource){
        this.messageSource = messageSource;
    }

    protected ResponseDTO<Long> createErrorResponse(BindingResult bindingResult) {
        ResponseDTO<Long> responseDTO = new ResponseDTO<Long>();
        bindingResult.getAllErrors().forEach(error -> {
            FieldError fieldError = (FieldError) error;
            String fieldName = fieldError.getField();

            responseDTO.addError(fieldName, messageSource.getMessage(fieldError.getDefaultMessage(), null, Locale.getDefault()));
        });

        return responseDTO;
    }
}
