package hr.zeroinfinity.focustowin.data.repository;

import hr.zeroinfinity.focustowin.data.model.Level;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LevelRepository extends JpaRepository<Level, Long> {
}