package hr.zeroinfinity.focustowin.data.repository;

import hr.zeroinfinity.focustowin.data.model.Chore;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ChoreRepository extends JpaRepository<Chore,Long> {
}
