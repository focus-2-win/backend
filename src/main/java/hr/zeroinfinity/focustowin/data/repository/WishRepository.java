package hr.zeroinfinity.focustowin.data.repository;

import hr.zeroinfinity.focustowin.data.model.Wish;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WishRepository extends JpaRepository<Wish, Long> {
}
