package hr.zeroinfinity.focustowin.data.repository;

import hr.zeroinfinity.focustowin.data.model.Event;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface EventRepository extends JpaRepository<Event,Long>, PagingAndSortingRepository<Event, Long>{

    @Query("select e from Event e inner join e.wish w where w.childId = (:childId) order by e.startTime desc")
    Page<Event> getEventByChild(@Param(("childId")) Long childId, Pageable pageable);

    @Query("select  e from  Event e inner join e.wish w where w.childId = (:childId) and e.startTime > (:from) and e.startTime < (:to)")
    List<Event> getEventByChildInRange(@Param("childId") Long childId, @Param("from") Date from, @Param("to") Date to);


}
