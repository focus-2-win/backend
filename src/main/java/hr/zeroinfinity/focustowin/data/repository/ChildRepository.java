package hr.zeroinfinity.focustowin.data.repository;

import hr.zeroinfinity.focustowin.data.model.Child;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ChildRepository extends JpaRepository<Child,Long> {
}
