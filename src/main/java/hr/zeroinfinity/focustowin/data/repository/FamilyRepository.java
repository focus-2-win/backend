package hr.zeroinfinity.focustowin.data.repository;

import hr.zeroinfinity.focustowin.data.model.Child;
import hr.zeroinfinity.focustowin.data.model.Family;
import hr.zeroinfinity.focustowin.data.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FamilyRepository extends JpaRepository<Family, Long> {

    Optional<Family> findByParentsContaining(User parent);

    Optional<Family> findByMinorsContaining(Child child);
}
