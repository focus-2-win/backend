package hr.zeroinfinity.focustowin.data.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ChoreStatusEnum {
    NOT_COMPLETED(1,"NOT_COMPLETED"),COMPLETED(2,"COMPLETED");

    private Integer id;
    private String name;
}
