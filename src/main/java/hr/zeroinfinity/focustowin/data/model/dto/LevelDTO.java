package hr.zeroinfinity.focustowin.data.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
@NoArgsConstructor
public class LevelDTO implements Serializable {

    @NotBlank(message = "level.id.blank")
    private Long id;

    private Integer minPoints;

    private Integer maxPoints;

    private Long multiplier;
}
