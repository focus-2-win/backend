package hr.zeroinfinity.focustowin.data.model.dto;

import hr.zeroinfinity.focustowin.data.model.ChoreStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChoreDTO {

    private Long id;

    private Integer points;

    @NotBlank(message = "chores.name.blank")
    private String name;

    private ChoreStatus choreStatus;

    private Long eventId;

}