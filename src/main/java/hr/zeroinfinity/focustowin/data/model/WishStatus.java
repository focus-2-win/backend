package hr.zeroinfinity.focustowin.data.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Data
@NoArgsConstructor
@Entity
@Table(name = "wish_statuses")
public class WishStatus implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id")
    @Setter(AccessLevel.PRIVATE)
    private Integer id;

    @Column(name = "name", updatable = false)
    private String name;

    @JsonIgnore
    @OneToMany(mappedBy = "status", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<Wish> wishes;

    public WishStatus(WishStatusEnum wishStatusEnum){
        this.id = wishStatusEnum.getId();
        this.name = wishStatusEnum.getName();
    }

    @Override
    public int hashCode(){
        return Objects.hashCode(id);
    }

    @Override
    public boolean equals(Object obj){
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        WishStatus other = (WishStatus) obj;
        return Objects.equals(id,other.getId());
    }
}
