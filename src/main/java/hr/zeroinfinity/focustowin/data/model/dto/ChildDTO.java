package hr.zeroinfinity.focustowin.data.model.dto;

import hr.zeroinfinity.focustowin.data.model.Level;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public class ChildDTO implements Serializable {

    private static final long serialVersionUID = 4924994636445060998L;

    @NotBlank(message = "child.username.blank")
    private String username;

    private Long id;

    private Integer avatarId;

    private Integer pointsEarned;

    private LevelDTO level;

    private List<WishDTO> wishList = new ArrayList<>();

    private Integer lastWeekTasksCount;

    private Integer lastWeekTasksCompletedCount;
}
