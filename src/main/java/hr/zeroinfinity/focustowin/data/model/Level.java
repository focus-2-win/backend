package hr.zeroinfinity.focustowin.data.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Data
@NoArgsConstructor
@Entity
@Table(name = "levels")
public class Level implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "min_points", nullable = false)
    private Integer minPoints;

    @Column(name = "max_points", nullable = false)
    private Integer maxPoints;

    @OneToMany(mappedBy = "level", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<Child> children;

    public Level(long id, int minPoints, int maxPoints) {
        this.id = id;
        this.minPoints = minPoints;
        this.maxPoints = maxPoints;
    }

    @Override
    public int hashCode(){
        return Objects.hashCode(id);
    }

    @Override
    public boolean equals(Object obj){
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        Level other = (Level) obj;
        return Objects.equals(id,other.getId());
    }

}
