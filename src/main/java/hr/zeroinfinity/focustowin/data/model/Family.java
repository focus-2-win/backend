package hr.zeroinfinity.focustowin.data.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Data
@AllArgsConstructor
@Entity
@Table(name = "families")
public class Family implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final String SEQUENCE_NAME = "family_sequence";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "family_name")
    private String familyName;

    @OneToMany(mappedBy = "family", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<User> parents;

    @OneToMany(mappedBy = "family", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<Child> minors;

    @Override
    public int hashCode(){
        return Objects.hashCode(id);
    }

    @Override
    public boolean equals(Object obj){
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        Family other = (Family) obj;
        return Objects.equals(id,other.getId());
    }

    public Family(){
        this.minors = new ArrayList<>();
        this.parents = new ArrayList<>();
    }

}

