package hr.zeroinfinity.focustowin.data.model;


import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum WishStatusEnum {
    PENDING(1,"PENDING"), ACCEPTED(2, "ACCEPTED"), FULFILED(3, "FULFILLED");

    private Integer id;
    private String name;
}
