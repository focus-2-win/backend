package hr.zeroinfinity.focustowin.data.model.dto;

import hr.zeroinfinity.focustowin.data.model.User;
import hr.zeroinfinity.focustowin.util.SecurityContextUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {

    private Long id;

    @NotBlank(message = "user.email.blank")
    @Email(regexp = "^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$", message = "user.email.format")
    private String email;

    @NotBlank(message = "user.name.blank")
    private String name;

    @NotBlank(message = "user.surname.blank")
    private String surname;

    @NotBlank(message = "user.password.blank")
    @Size(min = 8, max = 56, message = "user.password.size")
    private String password;

    private Boolean isFamilyAdmin;


    public User toUser() {
        return new User(
                email,
                name,
                surname,
                SecurityContextUtil.getPasswordEncoder().encode(password));
    }
}
