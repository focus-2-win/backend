package hr.zeroinfinity.focustowin.data.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EventStatusEnum {
    FAILED(3, "FAILED"), SUCCEEDED(2, "SUCCEEDED"), PENDING(1,"PENDING");

    private final Integer id;
    private final String name;
}
