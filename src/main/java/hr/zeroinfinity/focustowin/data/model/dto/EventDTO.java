package hr.zeroinfinity.focustowin.data.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import hr.zeroinfinity.focustowin.data.model.EventStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EventDTO {

    private Long id;

    @NotNull
    private Long wishId;

    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;

    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    @NotNull
    private Long duration;

    private Integer points;

    private EventStatus status;

//    private WishStatus wishStatus;

    private List<ChoreDTO> chores;

//    private Long multiplier;

}