package hr.zeroinfinity.focustowin.data.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FamilyDTO {

    private Long id;

    @NotBlank(message = "family.parents.blank")
    private List<UserDTO> parents;

//    @NotBlank(message = "family.children.blank")
    private List<ChildDTO> minors;
}
