package hr.zeroinfinity.focustowin.data.model.dto;

import hr.zeroinfinity.focustowin.data.model.WishStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class WishDTO {

    private Long id;

    private Integer currentPoints;

    private Integer targetPoints;

    @NotBlank(message = "wish.description.blank")
    private String description;

    private String imageUrl;

    @NotNull(message = "request.entityId.null")
    private Long childId;

    private WishStatus wishStatus;

}
