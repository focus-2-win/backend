package hr.zeroinfinity.focustowin.data.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseDTO<T> implements Serializable {

    private static final long serialVersionUID = -8281727139091998759L;
    private static final String DEFAULT_MESSAGE = "OK";
    private static final Integer DEFAULT_STATUS = 200;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer status = DEFAULT_STATUS;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Map<String, String> errors = new HashMap<>();

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String message = DEFAULT_MESSAGE;

    private Boolean valid;

    private T entityId;

    public ResponseDTO(T entityId){
        this.entityId = entityId;
    }

    public void addError(String name, String description) {
        status = HttpStatus.UNPROCESSABLE_ENTITY.value();
        errors.put(name, description);
    }

}
