package hr.zeroinfinity.focustowin.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class MathUtils {

    @Value("${util.math.points-constant}")
    private int constant;

    public int calculatePointesEarnedOnEvent( Long duration){
        long result = constant * duration;
        return (int) result;
    }
}
