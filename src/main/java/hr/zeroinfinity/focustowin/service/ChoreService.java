package hr.zeroinfinity.focustowin.service;

import hr.zeroinfinity.focustowin.data.model.Chore;
import hr.zeroinfinity.focustowin.data.model.dto.ChoreDTO;

public interface ChoreService {

    ChoreDTO updateChore(ChoreDTO choreDTO);

    void deleteChore(Long id);

    void saveChore(Chore chore);

    Chore findChoreById(Long id);
}

