package hr.zeroinfinity.focustowin.service;


import hr.zeroinfinity.focustowin.data.model.Wish;
import hr.zeroinfinity.focustowin.data.model.dto.WishDTO;

public interface WishService {

    WishDTO createWish(WishDTO wishDTO);

    WishDTO updateWish(WishDTO wishDTO);

    void deleteWish(Long id);

    void saveWish(Wish wish);
}
