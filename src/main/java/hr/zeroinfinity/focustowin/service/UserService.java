package hr.zeroinfinity.focustowin.service;

import hr.zeroinfinity.focustowin.data.model.User;
import hr.zeroinfinity.focustowin.data.model.dto.UserDTO;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

    void saveUser(User user);

    UserDTO register(UserDTO user);

    User update(UserDTO user);

    User findById(Long id);
}