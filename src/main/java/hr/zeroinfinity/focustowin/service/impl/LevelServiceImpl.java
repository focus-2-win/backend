package hr.zeroinfinity.focustowin.service.impl;

import hr.zeroinfinity.focustowin.config.exception.EntityNotFoundException;
import hr.zeroinfinity.focustowin.data.model.Child;
import hr.zeroinfinity.focustowin.data.model.Level;
import hr.zeroinfinity.focustowin.data.repository.LevelRepository;
import hr.zeroinfinity.focustowin.service.LevelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static hr.zeroinfinity.focustowin.config.exception.supplier.ExceptionSuppliers.ENTITY_NOT_FOUND;

@Service
public class LevelServiceImpl implements LevelService {

    private LevelRepository levelRepository;
    private static int POINTS_PER_LEVEL = 3000;

    @Autowired
    public LevelServiceImpl(LevelRepository levelRepository){
        this.levelRepository = levelRepository;
    }

    @Override
    public Level findByIdAndCreateIfNeeded(Long id) {
        if (id.equals(1L)) {
            return levelRepository.findById(1L).orElseThrow(ENTITY_NOT_FOUND);
        } else {
            Level oldLevel = levelRepository.findById(id -1 ).orElseThrow(ENTITY_NOT_FOUND);
            return levelRepository.findById(id).orElse(levelRepository.save(new Level(id, oldLevel.getMaxPoints(), oldLevel.getMaxPoints() + POINTS_PER_LEVEL)));
        }
    }

    @Override
    public void saveNewLevel(Level level){
        levelRepository.save(level);
    }
}
