package hr.zeroinfinity.focustowin.service.impl;

import hr.zeroinfinity.focustowin.config.exception.supplier.ExceptionSuppliers;
import hr.zeroinfinity.focustowin.data.model.User;
import hr.zeroinfinity.focustowin.data.model.dto.UserDTO;
import hr.zeroinfinity.focustowin.data.repository.UserRepository;
import hr.zeroinfinity.focustowin.service.AbstractService;
import hr.zeroinfinity.focustowin.service.UserService;
import hr.zeroinfinity.focustowin.service.component.mapper.impl.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

import static hr.zeroinfinity.focustowin.config.exception.supplier.ExceptionSuppliers.ENTITY_NOT_FOUND;

@Service
@Primary
public class UserServiceImpl extends AbstractService implements UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, UserMapper userMapper) {
        super(userRepository);
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }

    @Override
    public void saveUser(User user){
        userRepository.save(user);
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(email).orElseThrow(ExceptionSuppliers.USERNAME_NOT_FOUND);
        Set<GrantedAuthority> auth = new HashSet<>();

        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPasswordHash().split("}")[1], auth);
    }

    @Override
    public UserDTO register(UserDTO userDTO) {
        User user = userMapper.dtoToEntity(userDTO);
        return userMapper.entityToDto(userRepository.save(user));
    }

    @Override
    public User update(UserDTO userDTO) {
        User user = getActiveUser();

        user.setName(userDTO.getName());
        user.setSurname(userDTO.getSurname());

        return userRepository.save(user);
    }

    @Override
    public User findById(Long id){
        return userRepository.findById(id).orElseThrow(ENTITY_NOT_FOUND);
    }
}
