package hr.zeroinfinity.focustowin.service.impl;

import hr.zeroinfinity.focustowin.data.model.Child;
import hr.zeroinfinity.focustowin.data.model.Level;
import hr.zeroinfinity.focustowin.data.model.dto.ChildDTO;
import hr.zeroinfinity.focustowin.data.repository.ChildRepository;
import hr.zeroinfinity.focustowin.data.repository.UserRepository;
import hr.zeroinfinity.focustowin.service.AbstractService;
import hr.zeroinfinity.focustowin.service.ChildService;
import hr.zeroinfinity.focustowin.service.LevelService;
import hr.zeroinfinity.focustowin.service.component.mapper.impl.ChildMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

import static hr.zeroinfinity.focustowin.config.exception.supplier.ExceptionSuppliers.ENTITY_NOT_FOUND;

@Service
public class ChildServiceImpl extends AbstractService implements ChildService {

    private final ChildRepository childRepository;
    private final LevelService levelService;
    private final ChildMapper childMapper;

    @Autowired
    public ChildServiceImpl(UserRepository userRepository, ChildRepository childRepository, LevelService levelService, ChildMapper childMapper) {
        super(userRepository);

        this.childRepository = childRepository;
        this.levelService = levelService;
        this.childMapper = childMapper;
    }

    @Override
    public ChildDTO fetchChild(Long childId){
        Child child = childRepository.findById(childId).orElseThrow(ENTITY_NOT_FOUND);
        return childMapper.entityToDto(child);
    }

    @Override
    public ChildDTO createChild(){
        Child child = new Child();
        Level level  = levelService.findByIdAndCreateIfNeeded(1L);

        child.setLevel(level);
        child.setPointsEarned(0);
        child.setWishList(new ArrayList<>());
        childRepository.save(child);

        return childMapper.entityToDto(child);
    }

    @Override
    public void removeChild(Long childId) {
        Child deletedChild = childRepository.findById(childId).orElseThrow(ENTITY_NOT_FOUND);
        childRepository.delete(deletedChild);
    }

    @Override
    public void giveChildPoints(Child child, Integer points) {
        child.setPointsEarned(child.getPointsEarned() + points);
        if (child.getPointsEarned() >= child.getLevel().getMaxPoints()) {
            Level newLevel = levelService.findByIdAndCreateIfNeeded(child.getLevel().getId() + 1L);
            levelService.saveNewLevel(newLevel);
        }
        saveChild(child);
    }

    @Override
    public ChildDTO updateChild(ChildDTO childDTO) {
        Long childId = childDTO.getId();

        Child child = childRepository.findById(childId).orElseThrow(ENTITY_NOT_FOUND);

        child.setUsername(childDTO.getUsername());
        child.setAvatarId(childDTO.getAvatarId());
        child.setFamily(getActiveUser().getFamily());
        Child newChild = childRepository.save(child);

        return childMapper.entityToDto(newChild);
    }

    @Override
    public Child findById(Long id){
        return childRepository.findById(id).orElseThrow(ENTITY_NOT_FOUND);
    }

    @Override
    public void saveChild(Child child){
        childRepository.save(child);
    }

}
