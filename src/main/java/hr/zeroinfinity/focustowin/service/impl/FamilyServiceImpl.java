package hr.zeroinfinity.focustowin.service.impl;

import hr.zeroinfinity.focustowin.data.model.Child;
import hr.zeroinfinity.focustowin.data.model.Family;
import hr.zeroinfinity.focustowin.data.model.User;
import hr.zeroinfinity.focustowin.data.model.dto.ChildDTO;
import hr.zeroinfinity.focustowin.data.model.dto.FamilyDTO;
import hr.zeroinfinity.focustowin.data.model.dto.UserDTO;
import hr.zeroinfinity.focustowin.data.repository.ChildRepository;
import hr.zeroinfinity.focustowin.data.repository.FamilyRepository;
import hr.zeroinfinity.focustowin.data.repository.UserRepository;
import hr.zeroinfinity.focustowin.service.AbstractService;
import hr.zeroinfinity.focustowin.service.ChildService;
import hr.zeroinfinity.focustowin.service.FamilyService;
import hr.zeroinfinity.focustowin.service.UserService;
import hr.zeroinfinity.focustowin.service.component.mapper.impl.ChildMapper;
import hr.zeroinfinity.focustowin.service.component.mapper.impl.FamilyMapper;
import hr.zeroinfinity.focustowin.service.component.mapper.impl.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;

import static hr.zeroinfinity.focustowin.config.exception.supplier.ExceptionSuppliers.ENTITY_NOT_FOUND;

@Service
public class FamilyServiceImpl extends AbstractService implements FamilyService {

    private final FamilyRepository familyRepository;
    private final UserService userService;
    private final ChildService childService;
    private final FamilyMapper familyMapper;
    private final UserMapper userMapper;
    private final ChildMapper childMapper;

    @Autowired
    public FamilyServiceImpl(FamilyRepository familyRepository,UserRepository userRepository, UserService userService, ChildService childService, FamilyMapper familyMapper, UserMapper userMapper, ChildMapper childMapper) {
        super(userRepository);
        this.familyRepository = familyRepository;
        this.userService = userService;
        this.childService = childService;
        this.familyMapper = familyMapper;
        this.userMapper = userMapper;
        this.childMapper = childMapper;
    }

    @Override
    public FamilyDTO get(){
        return familyMapper.entityToDto(familyRepository.findByParentsContaining(getActiveUser()).orElseThrow(ENTITY_NOT_FOUND));
    }

    @Override
    public FamilyDTO create(){
        User user = getActiveUser();
        Family family = user.getFamily();

        if(family != null){
            throw new IllegalArgumentException();
        }

        family = new Family();
        family = familyRepository.save(family);

        user.setFamily(family);
        user.setIsFamilyAdmin(true);
        userService.saveUser(user);

        return familyMapper.entityToDto(familyRepository.save(family));
    }

    @Override
    public UserDTO addParent(Long userId) {
        User user = getActiveUser();
        Family family = user.getFamily();
        User newUser = userService.findById(userId);

        if(newUser.getFamily() != null){
            throw ENTITY_NOT_FOUND.get();
        }

        family.getParents().add(newUser);
        familyRepository.save(family);

        return userMapper.entityToDto(newUser);
    }

    @Override
    public ChildDTO addChild(ChildDTO childDTO){
        User user = getActiveUser();
        Family family = user.getFamily();

        Child child = childService.findById(childDTO.getId());
        if(child.getFamily() != null){
            throw ENTITY_NOT_FOUND.get();
        }

        child.setUsername(childDTO.getUsername());
        child.setAvatarId(childDTO.getAvatarId());
        child.setFamily(family);

        childService.saveChild(child);

        return childMapper.entityToDto(child);
    }

    @Override
    public FamilyDTO findFamilyByChild(Long childId){
        Child child = childService.findById(childId);
        Family f = familyRepository.findByMinorsContaining(child).orElseThrow(ENTITY_NOT_FOUND);
        return familyMapper.entityToDto(f);
    }
}
