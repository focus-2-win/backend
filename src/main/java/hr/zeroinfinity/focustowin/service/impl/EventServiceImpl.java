package hr.zeroinfinity.focustowin.service.impl;

import hr.zeroinfinity.focustowin.data.model.*;
import hr.zeroinfinity.focustowin.data.model.Chore;
import hr.zeroinfinity.focustowin.data.model.dto.ChoreDTO;
import hr.zeroinfinity.focustowin.data.model.dto.EventDTO;
import hr.zeroinfinity.focustowin.data.repository.ChildRepository;
import hr.zeroinfinity.focustowin.data.repository.ChoreRepository;
import hr.zeroinfinity.focustowin.data.repository.EventRepository;
import hr.zeroinfinity.focustowin.data.repository.UserRepository;
import hr.zeroinfinity.focustowin.data.repository.WishRepository;
import hr.zeroinfinity.focustowin.service.*;
import hr.zeroinfinity.focustowin.service.component.mapper.impl.ChoreMapper;
import hr.zeroinfinity.focustowin.service.component.mapper.impl.EventMapper;
import hr.zeroinfinity.focustowin.util.MathUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static hr.zeroinfinity.focustowin.config.exception.supplier.ExceptionSuppliers.ENTITY_NOT_FOUND_CHORE;
import static hr.zeroinfinity.focustowin.config.exception.supplier.ExceptionSuppliers.ENTITY_NOT_FOUND;

@Service
public class EventServiceImpl extends AbstractService implements EventService {

    private final EventRepository eventRepository;
    private final ChildService childService;
    private final WishService wishService;
    private final ChoreService choreService;
    private final EventMapper eventMapper;
    private final ChoreMapper choreMapper;
    private final MathUtils mathUtils;

    public EventServiceImpl(UserRepository userRepository , EventRepository eventRepository, EventMapper eventMapper, ChoreMapper choreMapper,ChoreService choreService, WishService wishService,  ChildService childService, MathUtils mathUtils){
        super(userRepository);
        this.eventRepository = eventRepository;
        this.childService = childService;
        this.wishService = wishService;
        this.choreService = choreService;
        this.eventMapper = eventMapper;
        this.choreMapper = choreMapper;
        this.mathUtils = mathUtils;
    }

    @Override
    public EventDTO createEvent(EventDTO eventDTO){
        Event event = eventMapper.dtoToEntity(eventDTO);
        event.setChores(new ArrayList<>());
        event.setStatus(new EventStatus(EventStatusEnum.PENDING));
        event = eventRepository.save(event);
        if(eventDTO.getChores() != null) {
            for (ChoreDTO choreDTO : eventDTO.getChores()) {
                Chore chore = choreMapper.dtoToEntity(choreDTO);
                ChoreStatus status = new ChoreStatus(ChoreStatusEnum.NOT_COMPLETED);
                chore.setStatus(status);
                chore.setEvent(event);
                choreService.saveChore(chore);
            }
        }
        return eventMapper.entityToDto(event);
    }

    @Override
    public EventDTO updateEvent(EventDTO eventDTO){
        Event event = eventRepository.findById(eventDTO.getId()).orElseThrow(ENTITY_NOT_FOUND);

        int earnedPoints = 0;
        Wish wish = event.getWish();
        Child child = wish.getChild();
        EventStatus success = new EventStatus(EventStatusEnum.SUCCEEDED);
        if(event.getStatus().equals(success)){
            earnedPoints = mathUtils.calculatePointesEarnedOnEvent(event.getDuration());
        }
        event.setPoints(earnedPoints);

        int earnedPerChore = 0;
        if(eventDTO.getChores() != null && !eventDTO.getChores().isEmpty()) {
            for (ChoreDTO choreDTO : eventDTO.getChores()) {
                if(choreDTO.getId() != null){
                    Chore chore = choreService.findChoreById(choreDTO.getId());
                    if (choreDTO.getChoreStatus().getId() == 2) {
                        earnedPerChore += chore.getPoints();
                        chore.setStatus(new ChoreStatus(ChoreStatusEnum.COMPLETED));
                    }
                    choreService.saveChore(chore);
                } else {
                    throw ENTITY_NOT_FOUND_CHORE.get();
                }
            }
        }

        event.setPoints(earnedPoints + earnedPerChore);
        int currentPointsWish = wish.getCurrentPoints();
        wish.setCurrentPoints(currentPointsWish + earnedPoints + earnedPerChore);
        if(wish.getCurrentPoints() > wish.getTargetPoints()){
            wish.setStatus(new WishStatus(WishStatusEnum.FULFILED));
        }

        childService.giveChildPoints(child,earnedPoints + earnedPerChore);
        wishService.saveWish(wish);
        eventRepository.save(event);

        return eventMapper.entityToDto(event);
    }

    @Override
    public EventDTO deleteEvent(Long id){
        EventDTO eventDTO = eventMapper.entityToDto(eventRepository.findById(id).orElseThrow(ENTITY_NOT_FOUND));
        eventRepository.deleteById(id);
        return eventDTO;
    }

    @Override
    public Page<EventDTO> getEventsForChild(Long childId, int page, int pageSize){
        Pageable pageable = PageRequest.of(page,pageSize, Sort.by("startTime"));
        Child child = childService.findById(childId);

        Page<Event> eventsByChild = eventRepository.getEventByChild(child.getId(), pageable);

        List<EventDTO> eventDTOS = new ArrayList<>();
        for(Event event : eventsByChild.getContent()){
            EventDTO tempEventDTO = eventMapper.entityToDto(event);
            List<ChoreDTO> choreDTOS = new ArrayList<>();
            for(Chore chore : event.getChores()){
                choreDTOS.add(choreMapper.entityToDto(chore));
            }
            tempEventDTO.setChores(choreDTOS);
            eventDTOS.add(tempEventDTO);
        }

        return new PageImpl<>(eventDTOS,pageable, eventsByChild.getTotalElements());
    }



}
