package hr.zeroinfinity.focustowin.service.impl;

import hr.zeroinfinity.focustowin.data.model.*;
import hr.zeroinfinity.focustowin.data.model.dto.WishDTO;
import hr.zeroinfinity.focustowin.data.repository.UserRepository;
import hr.zeroinfinity.focustowin.data.repository.WishRepository;
import hr.zeroinfinity.focustowin.service.AbstractService;
import hr.zeroinfinity.focustowin.service.ChildService;
import hr.zeroinfinity.focustowin.service.WishService;
import hr.zeroinfinity.focustowin.service.component.mapper.impl.WishMapper;
import org.springframework.stereotype.Service;

import static hr.zeroinfinity.focustowin.config.exception.supplier.ExceptionSuppliers.ENTITY_NOT_FOUND;

@Service
public class WishServiceImpl extends AbstractService implements WishService {

    private final WishRepository wishRepository;
    private final ChildService childService;
    private final WishMapper wishMapper;

    public WishServiceImpl(WishRepository wishRepository, WishMapper wishMapper, UserRepository userRepository, ChildService childService){
        super(userRepository);
        this.childService = childService;
        this.wishMapper = wishMapper;
        this.wishRepository = wishRepository;
    }

    @Override
    public WishDTO createWish(WishDTO wishDTO) {
        Wish wish = wishMapper.dtoToEntity(wishDTO);
        WishStatus status = new WishStatus(WishStatusEnum.PENDING);
        wish.setStatus(status);
        wish.setCurrentPoints(0);
        wish = wishRepository.save(wish);
        Child child = childService.findById(wishDTO.getChildId());
        child.getWishList().add(wish);
        childService.saveChild(child);
        return wishMapper.entityToDto(wish);
    }

    @Override
    public WishDTO updateWish(WishDTO wishDTO) {
        Wish wish = wishRepository.findById(wishDTO.getId()).orElseThrow(ENTITY_NOT_FOUND);
        wish.setDescription(wishDTO.getDescription());
        wish.setStatus(wishDTO.getWishStatus());
        wish.setImageUrl(wishDTO.getImageUrl());
        wish.setTargetPoints(wishDTO.getTargetPoints());
        wish.setCurrentPoints(wishDTO.getCurrentPoints());
        wish.setChild(childService.findById(wishDTO.getChildId()));
        wishRepository.save(wish);
        return wishMapper.entityToDto(wish);
    }

    @Override
    public void deleteWish(Long id) {
        Wish wish = wishRepository.findById(id).orElseThrow(ENTITY_NOT_FOUND);
        wishRepository.delete(wish);
    }

    @Override
    public void saveWish(Wish wish){
        wishRepository.save(wish);
    }
}
