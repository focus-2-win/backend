package hr.zeroinfinity.focustowin.service.impl;

import hr.zeroinfinity.focustowin.config.exception.EntityNotFoundException;
import hr.zeroinfinity.focustowin.data.model.Chore;
import hr.zeroinfinity.focustowin.data.model.dto.ChoreDTO;
import hr.zeroinfinity.focustowin.data.repository.ChoreRepository;
import hr.zeroinfinity.focustowin.data.repository.UserRepository;
import hr.zeroinfinity.focustowin.service.AbstractService;
import hr.zeroinfinity.focustowin.service.ChoreService;
import org.springframework.stereotype.Service;

import static hr.zeroinfinity.focustowin.config.exception.supplier.ExceptionSuppliers.ENTITY_NOT_FOUND;


@Service
public class ChoreServiceImpl extends AbstractService implements ChoreService {

    private ChoreRepository choreRepository;


    public ChoreServiceImpl(UserRepository userRepository, ChoreRepository choreRepository){
        super(userRepository);
        this.choreRepository = choreRepository;
    }

    @Override
    public ChoreDTO updateChore(ChoreDTO choreDTO) {
        Chore chore = choreRepository.findById(choreDTO.getId()).orElseThrow(EntityNotFoundException::new);

        chore.setName(choreDTO.getName());
        chore.setPoints(choreDTO.getPoints());
        chore.setStatus(choreDTO.getChoreStatus());
        choreRepository.save(chore);

        return choreDTO;
    }

    @Override
    public void deleteChore(Long id) {
        choreRepository.deleteById(id);
    }

    @Override
    public void saveChore(Chore chore){
        choreRepository.save(chore);
    }

    @Override
    public Chore findChoreById(Long id) {
       return choreRepository.findById(id).orElseThrow(ENTITY_NOT_FOUND);
    }
}
