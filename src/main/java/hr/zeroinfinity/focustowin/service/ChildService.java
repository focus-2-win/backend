package hr.zeroinfinity.focustowin.service;

import hr.zeroinfinity.focustowin.data.model.Child;
import hr.zeroinfinity.focustowin.data.model.dto.ChildDTO;


public interface ChildService {

    void saveChild(Child child);

    ChildDTO createChild();

    void removeChild(Long childId);

    ChildDTO fetchChild(Long childId);

    ChildDTO updateChild(ChildDTO childDTO);

    void giveChildPoints(Child child, Integer points);

    Child findById(Long id);
}
