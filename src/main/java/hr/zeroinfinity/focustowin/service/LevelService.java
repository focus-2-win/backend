package hr.zeroinfinity.focustowin.service;

import hr.zeroinfinity.focustowin.data.model.Level;

public interface LevelService {

    Level findByIdAndCreateIfNeeded(Long id);

    void saveNewLevel(Level level);
}
