package hr.zeroinfinity.focustowin.service;

import hr.zeroinfinity.focustowin.data.model.User;
import hr.zeroinfinity.focustowin.data.repository.UserRepository;
import org.springframework.security.core.context.SecurityContextHolder;

import static hr.zeroinfinity.focustowin.config.exception.supplier.ExceptionSuppliers.*;

public class AbstractService {

    private final UserRepository userRepository;

    protected AbstractService(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    protected User getActiveUser(){
        return userRepository
                .findByEmail(SecurityContextHolder
                        .getContext()
                        .getAuthentication()
                        .getName()).orElseThrow(USERNAME_NOT_FOUND);
    }
}
