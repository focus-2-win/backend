package hr.zeroinfinity.focustowin.service;

import hr.zeroinfinity.focustowin.data.model.dto.ChildDTO;
import hr.zeroinfinity.focustowin.data.model.dto.FamilyDTO;
import hr.zeroinfinity.focustowin.data.model.dto.UserDTO;

public interface FamilyService {

    FamilyDTO create();

    UserDTO addParent(Long userId);

    FamilyDTO get();

    FamilyDTO findFamilyByChild(Long childId);

    ChildDTO addChild(ChildDTO childDTO);
}
