package hr.zeroinfinity.focustowin.service;

import hr.zeroinfinity.focustowin.data.model.dto.EventDTO;
import org.springframework.data.domain.Page;

public interface EventService {

    EventDTO createEvent(EventDTO eventDTO);

    EventDTO updateEvent(EventDTO eventDTO);

    EventDTO deleteEvent(Long id);

    Page<EventDTO> getEventsForChild(Long childId, int page, int pageSize);

}
