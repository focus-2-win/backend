package hr.zeroinfinity.focustowin.service.component.mapper.impl;

import hr.zeroinfinity.focustowin.data.model.Chore;
import hr.zeroinfinity.focustowin.data.model.dto.ChoreDTO;
import hr.zeroinfinity.focustowin.service.component.mapper.Mapper;
import org.springframework.stereotype.Component;

@Component
public class ChoreMapper implements Mapper<Chore, ChoreDTO> {

    @Override
    public ChoreDTO entityToDto(Chore entity) {
        ChoreDTO choreDTO = new ChoreDTO();
        choreDTO.setEventId(entity.getEventId());
        choreDTO.setId(entity.getId());
        choreDTO.setName(entity.getName());
        choreDTO.setPoints(entity.getPoints());
        choreDTO.setChoreStatus(entity.getStatus());
        return choreDTO;
    }

    public Chore dtoToEntity(ChoreDTO dto) {
        Chore chore = new Chore();
        chore.setId(dto.getId());
        chore.setName(dto.getName());
        chore.setPoints(dto.getPoints());
        chore.setStatus(dto.getChoreStatus());
        return chore;
    }
}