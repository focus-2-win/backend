package hr.zeroinfinity.focustowin.service.component.mapper.impl;

import hr.zeroinfinity.focustowin.data.model.Event;
import hr.zeroinfinity.focustowin.data.model.Wish;
import hr.zeroinfinity.focustowin.data.model.dto.EventDTO;
import hr.zeroinfinity.focustowin.data.repository.WishRepository;
import hr.zeroinfinity.focustowin.service.component.mapper.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static hr.zeroinfinity.focustowin.config.exception.supplier.ExceptionSuppliers.ENTITY_NOT_FOUND;

@Component
public class EventMapper implements Mapper<Event, EventDTO> {

    private final WishRepository wishRepository;

    @Autowired
    public EventMapper(WishRepository wishRepository) {
        this.wishRepository = wishRepository;
    }

    @Override
    public Event dtoToEntity(EventDTO dto){
        Event entity = new Event();

        entity.setStatus(dto.getStatus());
        Wish wish = wishRepository.findById(dto.getWishId()).orElseThrow(ENTITY_NOT_FOUND);
        entity.setWish(wish);
        entity.setStartTime(dto.getStartTime());
        entity.setEndTime(dto.getEndTime());
        entity.setDuration(dto.getDuration());
        entity.setPoints(dto.getPoints());

        return entity;
    }


    @Override
    public EventDTO entityToDto(Event entity){
        EventDTO dto = new EventDTO();
        dto.setId(entity.getId());
        dto.setStatus(entity.getStatus());
        dto.setDuration(entity.getDuration());
        dto.setStartTime(entity.getStartTime());
        dto.setEndTime(entity.getEndTime());
        dto.setWishId(entity.getWish().getId());
        dto.setPoints(entity.getPoints());
        return dto;
    }
}