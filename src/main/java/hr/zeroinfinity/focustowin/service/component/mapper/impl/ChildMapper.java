package hr.zeroinfinity.focustowin.service.component.mapper.impl;

import hr.zeroinfinity.focustowin.data.model.*;
import hr.zeroinfinity.focustowin.data.model.dto.ChildDTO;
import hr.zeroinfinity.focustowin.data.model.dto.EventDTO;
import hr.zeroinfinity.focustowin.data.model.dto.LevelDTO;
import hr.zeroinfinity.focustowin.data.model.dto.WishDTO;
import hr.zeroinfinity.focustowin.data.repository.EventRepository;
import hr.zeroinfinity.focustowin.service.component.mapper.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

@Component
public class ChildMapper implements Mapper<Child, ChildDTO>{

    private final WishMapper wishMapper;
    private final EventRepository eventRepository;

    @Autowired
    public ChildMapper(WishMapper wishMapper, EventRepository eventRepository) {
        this.wishMapper = wishMapper;
        this.eventRepository = eventRepository;
    }

    @Override
    public ChildDTO entityToDto(Child entity) {
        ChildDTO childDTO = new ChildDTO();
        childDTO.setId(entity.getId());
        childDTO.setUsername(entity.getUsername());
        childDTO.setAvatarId(entity.getAvatarId());
        childDTO.setPointsEarned(entity.getPointsEarned());

        LevelDTO levelDTO = createLevelDTOFromChild(entity);
        childDTO.setLevel(levelDTO);

        List<WishDTO> wishDTOList = createWishDTOListFromChild(entity);
        childDTO.setWishList(wishDTOList);

        List<Event> lastWeekTasks = createEventListFromChild(entity);
        int lastWeekCompletedTasksCount = getSuccesfullyCompletedEventsFromEventList(lastWeekTasks);
        childDTO.setLastWeekTasksCount(lastWeekTasks.size());
        childDTO.setLastWeekTasksCompletedCount(lastWeekCompletedTasksCount);

        return childDTO;
    }

    @Override
    public Child dtoToEntity(ChildDTO dto) {
        Child child = new Child();
        child.setUsername(dto.getUsername());
        child.setAvatarId(dto.getAvatarId());
        return child;
    }

    private LevelDTO createLevelDTOFromChild(Child entity){
        LevelDTO levelDTO = new LevelDTO();
        levelDTO.setId(entity.getLevel().getId());
        levelDTO.setMinPoints(entity.getLevel().getMinPoints());
        levelDTO.setMaxPoints(entity.getLevel().getMaxPoints());
        return  levelDTO;
    }

    private ArrayList<WishDTO> createWishDTOListFromChild(Child entity){
        ArrayList<WishDTO> wishDTOList = new ArrayList<>();
        if(entity.getWishList() != null) {
            for (Wish w : entity.getWishList()) {
                wishDTOList.add(wishMapper.entityToDto(w));
            }
        }
        return wishDTOList;
    }

    private List<Event> createEventListFromChild(Child entity){
        Date date = new Date();
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTime(date);
        gregorianCalendar.add(Calendar.DATE,-7);
        List<Event> lastWeekTasks =  new ArrayList<>();
        lastWeekTasks = eventRepository.getEventByChildInRange(entity.getId(), gregorianCalendar.getTime(),date);
        return lastWeekTasks;
    }

    private int getSuccesfullyCompletedEventsFromEventList(List<Event> list){
        List<Event> lastWeekCompletedTasks = new ArrayList<>();
        EventStatus success = new EventStatus(EventStatusEnum.SUCCEEDED);
        for(Event e : list){
            if(e.getStatus() ==  success){
                lastWeekCompletedTasks.add(e);
            }
        }
        return lastWeekCompletedTasks.size();
    }
}
