package hr.zeroinfinity.focustowin.service.component.mapper.impl;

import hr.zeroinfinity.focustowin.data.model.Child;
import hr.zeroinfinity.focustowin.data.model.Family;
import hr.zeroinfinity.focustowin.data.model.User;
import hr.zeroinfinity.focustowin.data.model.dto.ChildDTO;
import hr.zeroinfinity.focustowin.data.model.dto.FamilyDTO;
import hr.zeroinfinity.focustowin.data.model.dto.UserDTO;
import hr.zeroinfinity.focustowin.service.component.mapper.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class FamilyMapper implements Mapper<Family, FamilyDTO> {

    private UserMapper userMapper;
    private ChildMapper childMapper;

    @Autowired
    public FamilyMapper(UserMapper userMapper, ChildMapper childMapper){
        this.userMapper = userMapper;
        this.childMapper = childMapper;
    }

    @Override
    public FamilyDTO entityToDto(Family entity) {
        List<UserDTO> userDTOS = new ArrayList<>();
        List<ChildDTO> childDTOS = new ArrayList<>();
        if(entity.getParents() != null && entity.getMinors() != null) {
            for (User u : entity.getParents()) {
                userDTOS.add(userMapper.entityToDto(u));
            }
            for (Child c : entity.getMinors()) {
                childDTOS.add(childMapper.entityToDto(c));
            }
        }
        FamilyDTO dto = new FamilyDTO();
        dto.setId(entity.getId());
        dto.setParents(userDTOS);
        dto.setMinors(childDTOS);
        return dto;
    }

    @Override
    public Family dtoToEntity(FamilyDTO dto) {
        return null;
    }
}
