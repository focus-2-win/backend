package hr.zeroinfinity.focustowin.service.component.mapper.impl;

import hr.zeroinfinity.focustowin.data.model.User;
import hr.zeroinfinity.focustowin.data.model.dto.UserDTO;
import hr.zeroinfinity.focustowin.service.component.mapper.Mapper;
import hr.zeroinfinity.focustowin.util.SecurityContextUtil;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserMapper implements Mapper<User, UserDTO> {

    @Override
    public UserDTO entityToDto(User entity) {
        UserDTO dto = new UserDTO();
        dto.setId(entity.getId());
        dto.setEmail(entity.getEmail());
        dto.setName(entity.getName());
        dto.setSurname(entity.getSurname());
        dto.setIsFamilyAdmin(entity.getIsFamilyAdmin());
        return dto;
    }

    @Override
    public User dtoToEntity(UserDTO dto) {
        return new User(
                dto.getEmail(),
                dto.getName(),
                dto.getSurname(),
                SecurityContextUtil.getPasswordEncoder().encode(dto.getPassword()));
    }
}
