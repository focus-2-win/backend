package hr.zeroinfinity.focustowin.service.component.mapper.impl;

import hr.zeroinfinity.focustowin.config.exception.EntityNotFoundException;
import hr.zeroinfinity.focustowin.data.model.Wish;
import hr.zeroinfinity.focustowin.data.model.dto.WishDTO;
import hr.zeroinfinity.focustowin.data.repository.ChildRepository;
import hr.zeroinfinity.focustowin.data.repository.WishRepository;
import hr.zeroinfinity.focustowin.service.component.mapper.Mapper;
import org.springframework.stereotype.Component;

import static hr.zeroinfinity.focustowin.config.exception.supplier.ExceptionSuppliers.ENTITY_NOT_FOUND;


@Component
public class WishMapper implements Mapper<Wish, WishDTO> {

    private final ChildRepository childRepository;
    private final WishRepository wishRepository;

    public WishMapper(ChildRepository childRepository, WishRepository wishRepository){
        this.childRepository = childRepository;
        this.wishRepository = wishRepository;
    }

    @Override
    public WishDTO entityToDto(Wish entity) {
        WishDTO result = new WishDTO();
        result.setId(entity.getId());
        result.setCurrentPoints(entity.getCurrentPoints());
        result.setTargetPoints(entity.getTargetPoints());
        result.setDescription(entity.getDescription());
        result.setWishStatus(entity.getStatus());
        result.setImageUrl(entity.getImageUrl());
        result.setChildId(entity.getChild().getId());
        return result;
    }

    @Override
    public Wish dtoToEntity(WishDTO dto) {
        Wish wish = new Wish();
        wish.setChild(childRepository.findById(dto.getChildId()).orElseThrow(ENTITY_NOT_FOUND));
        wish.setCurrentPoints(dto.getCurrentPoints());
        wish.setTargetPoints(dto.getTargetPoints());
        wish.setImageUrl(dto.getImageUrl());
        wish.setDescription(dto.getDescription());
        wish.setStatus(dto.getWishStatus());
        return wish;
    }
}
