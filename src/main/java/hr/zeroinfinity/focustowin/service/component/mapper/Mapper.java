package hr.zeroinfinity.focustowin.service.component.mapper;

public interface Mapper<E, D> {

    D entityToDto(E entity);

    E dtoToEntity(D dto);
}
