package hr.zeroinfinity.focustowin.service.impl;

import hr.zeroinfinity.focustowin.config.exception.EntityNotFoundException;
import hr.zeroinfinity.focustowin.data.model.Child;
import hr.zeroinfinity.focustowin.data.model.Family;
import hr.zeroinfinity.focustowin.data.model.Level;
import hr.zeroinfinity.focustowin.data.model.dto.FamilyDTO;
import hr.zeroinfinity.focustowin.data.repository.FamilyRepository;
import hr.zeroinfinity.focustowin.data.repository.UserRepository;
import hr.zeroinfinity.focustowin.service.ChildService;
import hr.zeroinfinity.focustowin.service.FamilyService;
import hr.zeroinfinity.focustowin.service.UserService;
import hr.zeroinfinity.focustowin.service.component.mapper.impl.ChildMapper;
import hr.zeroinfinity.focustowin.service.component.mapper.impl.FamilyMapper;
import hr.zeroinfinity.focustowin.service.component.mapper.impl.UserMapper;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static hr.zeroinfinity.focustowin.config.exception.supplier.ExceptionSuppliers.ENTITY_NOT_FOUND;


@RunWith(MockitoJUnitRunner.class)
public class FamilyServiceImplTest {

    private FamilyService familyService;

    @Mock
    private FamilyRepository familyRepository;
    @Mock
    private ChildService childService;
    @Mock
    private UserService userService;
    @Mock
    private UserRepository userRepository;
    @Mock
    private FamilyMapper familyMapper;
    @Mock
    private UserMapper userMapper;
    @Mock
    private ChildMapper childMapper;

    @Before
    public void setUp(){
        this.familyService = new FamilyServiceImpl(familyRepository,userRepository,userService,childService,familyMapper,userMapper,childMapper);
    }

    @Test
    public void findFamilyByChild1(){
        Child child = createChild();
        FamilyDTO familyDTO = new FamilyDTO();
        familyDTO.setId(5L);
        Mockito.when(childService.findById(anyLong())).thenReturn(child);
        Mockito.when(familyRepository.findByMinorsContaining(any(Child.class))).thenReturn(Optional.of(new Family()));
        Mockito.when(familyMapper.entityToDto(any(Family.class))).thenReturn(familyDTO);
        FamilyDTO familyByChild = familyService.findFamilyByChild(child.getId());

        Assert.assertTrue(familyByChild != null);
        Assert.assertEquals(Long.valueOf(5), familyByChild.getId());
    }

    private static Child createChild(){
        Child child = new Child();
        child.setId(1L);
        child.setUsername("Pero");
        child.setAvatarId(1);
        child.setWishList(new ArrayList<>());
        child.setLevel(new Level());
        child.setPointsEarned(0);
        child.setFamilyId(1L);
        Family family = new Family();
        family.getMinors().add(child);
        child.setFamily(family);
        return child;
    }
}
